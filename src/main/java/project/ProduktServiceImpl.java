package project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ProduktService")
public class ProduktServiceImpl implements ProduktService{

	public ProduktRepository produktRepository;
	
	@Autowired
	public ProduktServiceImpl(ProduktRepository produktRepository2)
	{
		this.produktRepository = produktRepository2;
	}

	@Override
	public Iterable<Produkt> findAll() {
		return produktRepository.findAll();
	}
	
	@Override
	public Produkt save(Produkt entity) {
		return produktRepository.save(entity);
	}
	
	@Override
	public void delete(int id) {
		produktRepository.delete(id);
	}
	
	@Override
	public Produkt find(int id) {
		 return produktRepository.findOne(id);
	}
}
