package project;

import java.util.Spliterator;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ProduktController 
{
	private final ProduktService produktService;
	
	@Autowired
	public ProduktController(ProduktService produktService) {
		this.produktService = produktService;
	}
	
	@RequestMapping(value="/login")
	public String login()
	{	
		return "login";
	}
	
	@RequestMapping(value="/")
	public String start()
	{
		return "redirect:/alleProdukte";
	}
	
	@RequestMapping(value="/alleProdukte", method=RequestMethod.GET)
	public ModelAndView getUsers() {
		ModelAndView mav = new ModelAndView("alleProdukte");
		Spliterator<Produkt> it = produktService.findAll().spliterator();
		mav.addObject("alleProdukte", StreamSupport.stream(it, false).toArray() );
		
		return mav;
	}	

	
	@Secured("ROLE_USER")
	@RequestMapping(value="/produkt/{id}", method=RequestMethod.GET)
	public ModelAndView showUser(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("produkteAnschaun");
		
		Produkt user = produktService.find(id);
		mav.addObject(user);
		
		return mav;
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/produkt/{id}/del", method=RequestMethod.GET)
	public String deleteProdukt(@PathVariable int id) {
		
		produktService.delete(id);
		
		return "redirect:/alleProdukte";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/produkt/{id}/make", method=RequestMethod.GET)
	public ModelAndView editUser(@PathVariable int id) {
		ModelAndView mav = new ModelAndView("produkt");
		
		Produkt user = produktService.find(id);
		mav.addObject(user);
		
		return mav;
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/produkt/add", method=RequestMethod.GET)
	public ModelAndView addProd() {
		ModelAndView mav = new ModelAndView("add");
		
		Produkt por = new Produkt();
		mav.addObject(por);
		
		return mav;
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/produkt/{id}/edit", method=RequestMethod.GET)
	public String updateProdukt(@PathVariable int id, @RequestParam("name") String name, @RequestParam("beschreibung") String beschreibung, @RequestParam("kurzbesch") String kurzbesch, @RequestParam("anzahl") int anzahl, @RequestParam("preis") double preis) {
	
		Produkt produkt = produktService.find(id);
		produkt.setName(name);
		produkt.setAnzahl(anzahl);
		produkt.setBeschreibung(beschreibung);
		produkt.setKurzbesch(kurzbesch);
		produkt.setPreis(preis);
		produktService.save(produkt);
		
		return "redirect:/alleProdukte";
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value="/produkt/adds", method=RequestMethod.GET)
	public String addProdukt(@RequestParam("name") String name, @RequestParam("beschreibung") String beschreibung, @RequestParam("kurzbesch") String kurzbesch, @RequestParam("anzahl") int anzahl, @RequestParam("preis") double preis) {
	
		int id = 1;
		for(Produkt s : produktService.findAll())
		{
			id++;
		}
		
		if(name != null && beschreibung != null && kurzbesch != null && !name.isEmpty() && !beschreibung.isEmpty()&& !kurzbesch.isEmpty()) {
			Produkt produkt = new Produkt(id, name, beschreibung, kurzbesch, anzahl, preis);
			produktService.save(produkt);
		}
		return "redirect:/alleProdukte";
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value="/produkt/{id}/kauf", method=RequestMethod.GET)
	public String kaufenProdukt(@PathVariable int id) {
		
		Produkt produkt = produktService.find(id);
		produkt.setAnzahl(produkt.getAnzahl() - 1);
		if(produkt.getAnzahl() < 0)
			produkt.setAnzahl(0);
		
		produktService.save(produkt);
		
		return "redirect:/produkt/{id}";
	}
	
	
}
