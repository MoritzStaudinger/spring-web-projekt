package project;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Produkt 
{
	@Id
	@GeneratedValue
	private int id;
	
	@NotEmpty(message = "{validation.name.NotEmpty.message}")
	@Size(min = 2, max = 50, message = "{validation.firstname.name.message}")
	private String name;
		
	@NotEmpty(message = "{validation.name.NotEmpty.message}")
	private String beschreibung;
	
	@Size(min = 2, max = 50, message = "{validation.firstname.name.message}")
	private String kurzbesch;
		
	@NotNull
	private int anzahl;
	
	@NotNull
	private double preis;
	
	
	public Produkt()
	{}
	
	public Produkt(int id, String name, String beschreibung, String kurzbesch, int anzahl, double preis) 
	{
		this.id = id;
		this.name = name;
		this.beschreibung = beschreibung;
		this.kurzbesch = kurzbesch;
		this.anzahl = anzahl;
		this.preis = preis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getKurzbesch() {
		return kurzbesch;
	}

	public void setKurzbesch(String kurzbesch) {
		this.kurzbesch = kurzbesch;
	}

	public int getAnzahl() {
		return anzahl;
	}

	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	
}
